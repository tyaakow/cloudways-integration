<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        

		@section('title')
			<title>Gitlab Tutorial Project</title>
		@show




		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet" />

        <style>   

		* {
			box-sizing:border-box;  
		}

		#main {
			margin: 20px auto;
			max-width: 800px; 
			font-family: 'Source Sans Pro', sans-serif;
			font-weight:400;
		}

		#main .top {
			width:100%;
			text-align: center;
			margin:10px 0px;
			padding:20px 40px;
		}
		#main .top h1 {
			margin:20px 0px;
			padding:0px;
			font-size: 10rem;
			color:#aaa;
		}

		#main #menu ul {
			font-weight:600;
			text-align:center;
			margin:20px 0px 40px 0px;
		}

		#main #menu ul li {
			display:inline;
			margin:0px 8px;
		}

		#main #menu ul li a {
			color:#222;
			font-size: 1.4rem;
			text-decoration: none;
		}

		#main #menu ul li.active a,
		#main #menu ul li a:hover {
			color:#777;
			text-decoration: underline;
		}


        
        </style>
    </head>
    <body>

		<div id="main">
			<div class="top">
				@yield('headline')
			</div>

			@include('partials.menu')



			<div class="content">
				@yield('content')
			</div>
		</div>
    </body>
</html>

