<div id="menu">
    <ul>
        <li class="{{ request()->is('/') ? 'active' : '' }}">
            <a href="/">Home</a>
        </li>
        <li class="{{ request()->is('page2') ? 'active' : '' }}">
			<a href="/page2">Page 2</a>
        </li>
        <li class="{{ request()->is('page3') ? 'active' : '' }}">
		    <a href="/page3">Page 3</a>
        </li>
        <li class="{{ request()->is('about') ? 'active' : '' }}">
		    <a href="/about">About</a>
        </li>
    </ul>
</div>