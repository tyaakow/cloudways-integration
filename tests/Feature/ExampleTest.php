<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }


    public function testIndexView()
    {
        $view = $this->view('index');

        $view->assertSee('Homepage');
    }

    public function testAboutView()
    {
        $view = $this->view('about');

        $view->assertSee('About');
    }

}
